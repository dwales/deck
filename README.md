# Deck

## No code as yet!

This project is currently in the "concept" phase.
I have lots of ideas, which I will outline below, but don't expect anything solid for some time.
(It could be several years before anything usable appears.)
In the meantime, I will use this repository to store files full of links to useful resources, and experimental code.

If you come up with anything interesting in the interim, feel free to submit a pull request.


## Plans for the future:

* Minimalist interface
* Full keyboard control
    - Modal (Vim inspired)
* Song database
    - Indexed
    - Instant search
* Bible database
* Customisable slide backgrounds
* Markdown support
* Keynote, Powerpoint (and potentially other presentation software support)
    - On Mac, Keynote and Powerpoint can be controlled through Applescript.
    - If you drag or import a Keynote / Powerpoint file into Deck, then it will add a "presentation instance"
        * The presentation instance will be uniform across all presentation software -- esentially an abstraction of it.
        * The document is not actually _imported_. Rather, the following steps are followed:
            - File path is recorded
            - Keynote or Powerpoint is opened in the background (hidden if possible)
            - Keynote or Powerpoint are configured via AppleScript (or similar if porting to another OS)
            - Deck presentation continues as normal until Keynote/Powerpoint reached
            - Keynote/Powerpoint presentation started via AppleScript, and controlled from Deck using regular Deck controls, but behind the scenes utilises AppleScript.
            - When presentation is finished, Keynote/Powerpoint is closed, and Deck continues on as normal.
    - This has the advantage that presentations will keep their native transitions and formatting, without being messed up.
    - This method could also be used for videos, but it might be better to just include Mplayer or VLC inside Deck to ensure a robust playback experience.
    - It would be good to make this extensible through plugins so that anyone can get ANY presentation software working with Deck. e.g.
        * [Prezi](http://engineering.prezi.com/blog/2013/09/03/prezi-javascript-api/)
        * [Libre Office Impress (Scripting API)](https://help.libreoffice.org/Common/Scripting#To_assign_a_script_to_a_key_combination)
        * [Sozi](http://sozi.baierouge.fr/pages/10-about.html)
        * etc

Files containing relevant links will be created at some point, but for now, here are a few good ones:

Applescript:

* [AppleScript - The Language of Automation](http://macosxautomation.com/)
* [Scripting Bridge](http://www.macosxautomation.com/applescript/features/scriptingbridge.html)
* [Detail about Scripting Brige](https://developer.apple.com/library/mac/DOCUMENTATION/Cocoa/Conceptual/ScriptingBridgeConcepts/UsingScriptingBridge/UsingScriptingBridge.html#//apple_ref/doc/uid/TP40006104-CH4-SW1)
* [Making Cocoa Apps Scriptable](https://developer.apple.com/library/mac/DOCUMENTATION/Cocoa/Conceptual/ScriptableCocoaApplications/SApps_intro/SAppsIntro.html#//apple_ref/doc/uid/TP40002164)
* [More stuff about Scripting Bridge](http://www.macresearch.org/scripting-apps-without-applescript)

Search Kit (for indexed songs, and instant search stuff...)

* [Intro](https://developer.apple.com/library/mac/documentation/userexperience/conceptual/SearchKitConcepts/searchKit_intro/searchKit_intro.html)
* [Reference](https://developer.apple.com/library/mac/documentation/UserExperience/Reference/SearchKit/Reference/reference.html)
* [Tutorial Thing](http://nshipster.com/search-kit/)
* [Could maybe use simpler Spotlight API instead of Search Kit](https://developer.apple.com/library/mac/documentation/carbon/conceptual/spotlightquery/Concepts/Introduction.html)

Windows

* [Embedding Powerpoint in a Windows app (in case we ever get that far...)](http://www.codeproject.com/Articles/118676/Embedding-PowerPoint-presentation-player-into-a-WP)
* [More embedding Powerpoint in Windows](http://stackoverflow.com/questions/11360758/embedding-a-powerpoint-show-into-a-c-sharp-application)

